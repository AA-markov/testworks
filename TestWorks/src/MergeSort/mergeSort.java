package MergeSort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class mergeSort {
    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>();
        int size = 20;
        Random a = new Random();
        for (int i = 0; i < size; i++) {
            arr.add(a.nextInt(size * 5) + 1);
        }
        System.out.println(arr);
        Sort(arr, 1, arr.size());
        System.out.println(arr);
    }

    private static void Sort(List<Integer> array, int first, int last) {
        if (last == first) {
            return;
        }
        int a = (last + first) / 2;
        Sort(array, first, a);
        Sort(array, a + 1, last);
        Merge(array, first, a, last);
    }

    private static void Merge(List<Integer> array, int start, int mid, int end) {
        List<Integer> arr1 = new ArrayList<>();
        for (int i = start - 1; i < mid; i++) {
            arr1.add(array.get(i));
        }
        List<Integer> arr2 = new ArrayList<>();
        for (int i = mid; i < end; i++) {
            arr2.add(array.get(i));
        }
        List<Integer> tempArray = new ArrayList<>();
        int count1 = 0, count2 = 0;
        for (int i = 0; i < (arr1.size() + arr2.size()); i++) {
            if (count2 == arr2.size()) {
                tempArray.add(arr1.get(count1));
                count1++;
            } else if (count1 == arr1.size() || arr1.get(count1) > arr2.get(count2)) {
                tempArray.add(arr2.get(count2));
                count2++;
            } else {
                tempArray.add(arr1.get(count1));
                count1++;
            }
        }
        array.subList(start - 1, end).clear();
        for (int j = 0; j < tempArray.size(); j++) {
            array.add(start - 1 + j, tempArray.get(j));
        }
    }
}
